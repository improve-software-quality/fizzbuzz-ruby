# frozen_string_literal: true

require "spec_helper"
require_relative "../lib/fb_number"

describe FbNumber do
  it "should convert a Number to a FbNumber" do
    expect(42.to_fb_number).to be_a(FbNumber)
  end

  it "should work with many integers" do
    number = rand(0..1_000_000_000)

    expect(number.to_fb_number).to be_a(FbNumber)
  end

  let(:fizzing_numbers) { [FbNumber.fizzing, FbNumber.fizzing, FbNumber.fizzing] }
  let(:buzzing_numbers) { [FbNumber.buzzing, FbNumber.buzzing, FbNumber.buzzing] }
  let(:fizzbuzzing_numbers) { [FbNumber.fizzbuzzing, FbNumber.fizzbuzzing, FbNumber.fizzbuzzing] }

  it "should return 'fizz' for fizzing numbers" do
    fizzing_numbers.each do |multiple|
      expect(FbNumber.of_value(multiple).to_s).to eq("fizz")
    end
  end

  it "should return 'buzz' for buzzing numbers" do
    buzzing_numbers.each do |multiple|
      expect(FbNumber.of_value(multiple).to_s).to eq("buzz")
    end
  end

  it "should return 'fizzbuzz' for fizzing AND buzzing numbers" do
    fizzbuzzing_numbers.each do |multiple|
      expect(FbNumber.of_value(multiple).to_s).to eq("fizzbuzz")
    end
  end

  it "should return a fizzing number" do
    expect(FbNumber.fizzing.fizzes?).to be_truthy
  end

  it "should return a buzzing number" do
    expect(FbNumber.buzzing.buzzes?).to be_truthy
  end

  it "should return a fizzbuzzing number" do
    expect(FbNumber.fizzbuzzing.buzzes?).to be_truthy
    expect(FbNumber.fizzbuzzing.fizzes?).to be_truthy
  end
end
