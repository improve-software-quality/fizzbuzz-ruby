# frozen_string_literal: true

require "spec_helper"
require_relative "../lib/fizz_buzz"
require_relative "../lib/fb_number"

describe "FizzBuzz" do
  it "should print the elements of the range" do
    fizzbuzz = FizzBuzz.with_range(1..2)

    expect { fizzbuzz.call }.to output("1\n2\n").to_stdout
  end

  it "should write fizz for values that are multiples of 3" do
    fizz_number = FbNumber.fizzing

    fizzbuzz = FizzBuzz.with_one_number(fizz_number)

    expect { fizzbuzz.call }.to output("fizz\n").to_stdout
  end

  it "should write buzz for values that are multiples of 5" do
    buzz_number = FbNumber.buzzing

    fizzbuzz = FizzBuzz.with_one_number(buzz_number)

    expect { fizzbuzz.call }.to output("buzz\n").to_stdout
  end

  it "should write fizzbuzz for values that are multiples of 3 AND 5" do
    fizz_buzz_number = FbNumber.fizzbuzzing

    fizzbuzz = FizzBuzz.with_one_number(fizz_buzz_number)

    expect { fizzbuzz.call }.to output("fizzbuzz\n").to_stdout
  end

  it "should write number and fizz/buzz/fizzbuzz for the respective numbers" do
    fizzbuzz = FizzBuzz.with_range(1..15)

    expected_output = <<-RESULT
1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
    RESULT

    expect { fizzbuzz.call }.to output(expected_output).to_stdout
  end
end
