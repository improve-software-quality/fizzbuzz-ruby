# frozen_string_literal: true

require_relative "./fb_number"

# A class to run fizz buzz
class FizzBuzz
  attr_accessor :range

  def self.with_range(range)
    klass = FizzBuzz.new
    klass.range = range.map(&:to_fb_number)
    klass
  end

  def self.with_one_number(number)
    with_range([number])
  end

  def call
    range.each(&method(:puts))
  end
end
