# frozen_string_literal: true

# A value object to be used in FizzBuzz
class FbNumber
  attr_accessor :value

  RANGE = (1..1_000_000).freeze

  def self.of_value(value)
    klass = FbNumber.new
    klass.value = value
    klass
  end

  def self.fizzing
    value = RANGE.to_a.select do |number|
      number.to_fb_number.fizzes? && !number.to_fb_number.buzzes?
    end.sample
    of_value(value)
  end

  def self.buzzing
    value = RANGE.to_a.select do |number|
      number.to_fb_number.buzzes? && !number.to_fb_number.fizzes?
    end.sample
    of_value(value)
  end

  def self.fizzbuzzing
    value = RANGE.to_a.select do |number|
      number.to_fb_number.buzzes? && number.to_fb_number.fizzes?
    end.sample
    of_value(value)
  end

  def to_fb_number
    self
  end

  def to_s
    if fizzes? && buzzes?
      "fizzbuzz"
    elsif fizzes?
      "fizz"
    elsif buzzes?
      "buzz"
    else
      value.to_s
    end
  end

  def to_i
    value.to_i
  end

  def fizzes?
    (value.to_i % 3).zero?
  end

  def buzzes?
    (value.to_i % 5).zero?
  end
end

class Integer
  def to_fb_number
    FbNumber.of_value(self)
  end
end
